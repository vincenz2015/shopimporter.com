
import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from './../components/Login.vue'
import Signup from './../components/Signup.vue'
import Home from './../components/Home.vue'
import Dashboard from './../components/Dashboard.vue'

/* Use Router */
Vue.use(VueRouter)

const router = new VueRouter({
  hashbang: false,
  history: true,
  mode: 'history',
  base: '.',
  historyApiFallback: true,
  routes: [
    { path: '/', name: 'home', component: Home , meta: { requiresAuth: true }},
    { path: '/login', name: 'login', component: Login },
    { path: '/signup', name: 'signup', component: Signup },
    { path: '/dashboard', name: 'dashboard', component: Dashboard, meta: { requiresAuth: true }}
  ]
})

export default router
