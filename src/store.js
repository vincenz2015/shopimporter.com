import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
    state:{
        user:{
            signedIn: false,
            email : null,
            name : null,
            photo : null,
            userId : null
        }
    }
})

export default store