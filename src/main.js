import Vue from 'vue'
import router from './router'
import store from './store'
import App from './MainApp.vue'

// firebase configurations
const firebase = require('firebase/app')
require('firebase/auth')
require('firebase/database')

const config = {
  apiKey: 'AIzaSyD3PX14k-rvRufI2qTKElv3sLef1C1QZa8',
  authDomain: 'shopimporter.firebaseapp.com',
  databaseURL: 'https://shopimporter.firebaseio.com',
  projectId: 'shopimporter',
  storageBucket: 'shopimporter.appspot.com',
  messagingSenderId: '644289074063'
}

const FbApp = firebase.initializeApp(config)

firebase.auth().onAuthStateChanged(function(user) {
    store.state.user.signedIn = (user != null)
    if(user){
      store.state.user.email = user.email
      store.state.user.name = user.displayName
      store.state.user.photo  = user.photoURL
      store.state.user.userId = user.uid
      router.replace('dashboard')
    }else{
      router.replace('login')
    }
})

//routing with state in mind
router.beforeEach((to, from, next) => {
  const currentUser = store.state.user.signedIn
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth)
  if (requiresAuth && !currentUser) {
    next('/login')
  } else if (requiresAuth && currentUser) {
    next()
  } else if (!requiresAuth && currentUser) {
    next('/dashboard')
  } else {
    next()
  }
});

// eslint-disable-line
new Vue({
  el: '#root',
  router,
  store,
  render: (h) => h(App)
})

Vue.prototype.FireBase = FbApp
Vue.config.debug = true
Vue.config.devtools = true

