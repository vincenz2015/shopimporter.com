import firebase from 'firebase/app'

export function signIn(email, password) {
  // using firebase to sign in with email verification
  const promise = firebase.auth().signInWithEmailAndPassword(email, password)
  promise.catch(e => alert(e.code + ' - ' + e.message))
}

export function signInViaGoogle() {
  const provider = new firebase.auth.GoogleAuthProvider()
  const promise = firebase.auth().signInWithRedirect(provider)
  promise.catch(e => alert(e.code + ' - ' + e.message))
}

export function signOut() {
  const promise = firebase.auth().signOut()
  promise.catch(e => alert(e.code + ' - ' + e.message))
}
