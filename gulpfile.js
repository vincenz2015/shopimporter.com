var gulp = require('gulp'),
    browserSync = require('browser-sync').create(),
    fs = require('fs'),
    browserify = require('browserify'),
    watchify = require('watchify'),
    vueify = require('vueify'),
    exorcist = require('exorcist'),
    eslint = require('gulp-eslint'),
    connect = require('gulp-connect-php'),
    paths = {
        entryfile: 'main.js',
        bundlename: 'bundle',
        src: 'src',
        public: 'public',
        test: 'test',
        app: 'app'
    };

// run server
gulp.task('serve:php', function () {
    connect.server({}, function () {
        var bs = browserSync.init({
            proxy: 'localhost:8000',
            https: true,
            notify: false,
            reloadDebounce: 1000
        });
        /* Watch the src for changes, then re-lint and re-bundle */
        gulp.watch(paths.src + "/*.js").on('change', function () { gulp.run(['lint', '_bundle']); });
        /* Watch the app for changes, then reload browswer */
        gulp.watch(paths.app + "/*.js").on('change', browserSync.reload);
    });
});

gulp.task('_server', function () {
    var bs = browserSync.init({
        server: {
            baseDir: "./"
        },
        https: true,
        notify: false,
        reloadDebounce: 1000
    });
    /* Watch the src for changes, then re-lint and re-bundle */
    gulp.watch(paths.src + "/*.js").on('change', function () { gulp.run(['lint', '_bundle']); });
    /* Watch the app for changes, then reload browswer */
    gulp.watch(paths.app + "/*.js").on('change', browserSync.reload);
});

// copy files - [unused]
gulp.task('_copyfiles', function () {
    gulp.src(paths.src + '/**/*')
        .pipe(gulp.dest(paths.dist));
});

// bundle
gulp.task('_bundle', function () {
    // create necessary files and folder to build the application
    var fs = require('fs')
    if (!fs.existsSync(paths.app)) {
        fs.mkdirSync(paths.app);
    }

    if (!fs.existsSync(paths.app + '/' + paths.bundlename + '.js')) {
        fs.createWriteStream(paths.app + '/' + paths.bundlename + '.js');
    }

    if (!fs.existsSync(paths.app + '/' + paths.bundlename + '.css')) {
        fs.createWriteStream(paths.app + '/' + paths.bundlename + '.css');
    }

    browserify(paths.src + '/' + paths.entryfile, { debug: true })
        .plugin('vueify/plugins/extract-css', {
            out: paths.app + '/' + paths.bundlename + '.css'
        })
        .bundle()
        .pipe(fs.createWriteStream(paths.app + '/' + paths.bundlename + '.js'));
});

/* Public gulp tasks */
// lint
gulp.task('lint', function () {
    gulp.src(paths.src + '/**/*.js')
        .pipe(eslint())
        .pipe(eslint.format());
});
// serve
gulp.task('serve', ['lint', '_bundle', '_server']);